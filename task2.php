<?php
/**
 * Lab Exam
 * task2.php
 * Date: 9/28/2016
 * Time: 9:33 AM
 */

    $arr = array("hello", "world", "how", "are", "you");

/*
    echo ucfirst(strrev($arr[4]))." "; // ucfirst() function for Uppercase first string letter
    echo ucfirst(strrev($arr[3]))." ";  //strrev() function to reverse a string
    echo ucfirst(strrev($arr[2]))." ";
    echo ucfirst(strrev($arr[1]))." ";
    echo ucfirst(strrev($arr[0]))." ";
*/

	echo ucwords(strrev(implode(" ",$arr))); //this line was added!
